#! /usr/src/perl
use warnings;

# This is a program to understand what's happening
# with variables in perl.

$x = 42;
print 'The value of $x is ', $x, "\n";

print 'The value of $x is still ', $x, "\n";

$x = "Mary had a little lamb, yea yea, yooo";

print 'The value of $x now is: ', $x, "\n";

print "Another way to print \$x is $x\n";
print "Another one way to print \$x is \"$x\"\n";
