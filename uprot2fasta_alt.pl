#! /usr/bin/perl 
use strict;
use warnings;

######################################################
#						     #
#	Get a uniprot file and change it to a        # 	  
#	fasta file. In fasta header we want it	     #
#	to have the ID and first AC code. There      #
#	is no limit to how the sequence will be      #
#	presented on the fasta file. 1-line, 60      #
#		character lines, etc		     #
#						     #
######################################################

my $uniprot_input = 'ACE2_HUMAN.txt';
my $fasta_output = 'ACE2_HUMAN.fasta';

open my $in_fh, "<", $uniprot_input 
	or die "Could not open '$uniprot_input' - $!";
open my $out_fh, ">", $fasta_output
	or die "Could not open '$fasta_output' for input - $!";


while (my $line = <$in_fh>) # Starts the $uniprot_input from the start and read the input file line by line.
{
	# Adds the ID part of the header.
	if ($line =~ m/^ID\s+(\w+).*\n/)
	{
		print $out_fh ">$1 ";
	}

	# Adds the AC part of the header.
	if ($line =~ m/^AC\s+(\w+).*\n/)
	{
		print $out_fh "$1\n";
	}

	# Adds the sequence of the protein (located in SQ part of uniprot file)
	my $seq = '';

	if ($line =~ m/^\s{5}(.*)/s) #for 1-liner remove s in the end of the regex.
	{
		$seq .= $1;
	}
	
	# Remove the spaces from the sequence.
	$seq =~ s= ==g; #OR $seq =~ s/ //g;
	
	print $out_fh "$seq";

}

close $in_fh;
close $out_fh;
