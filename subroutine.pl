#! /usr/bin/perl
use warnings;
use strict;

# Subroutine that will get as an input a sequence and a motif that will be used to scan the sequence.

my ($input_seq, $motiff) = ask_data();

my $count = 0;

while ($input_seq =~ m/$motiff/g)
{
	$count++;
}

print "Your motiff was found ", $count, " times \n";

$count = 0;

while ($input_seq =~ m/$motiff/g)
{
	$count++;
	print "Site #", $count, ": Position place: ", $-[0], "\n";
}


sub ask_data{
	print "Enter the sequence you want to parse in capital letters: ";
	my $input_seq = <STDIN>;
	chomp $input_seq;

	print "Enter which motiff you want to scan for in capital letters: ";
	my $motiff = <STDIN>;
	chomp $motiff;

	return ($input_seq, $motiff);
}
