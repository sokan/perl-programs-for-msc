#! /usr/bin/perl
use warnings;
use strict;

#
# R-G-D pattern: print the whole sequence in fasta format with its ID and below it have program
# mention if it found the pattern and where.
# 

my $seqs_file = 'SARS_COV2.fasta';

open (my $in_fh, "<", $seqs_file)
	or die "Could not open '$seqs_file'";

my $line = <$in_fh>;
my %sequence = '';
my $id = '';

#make a hash of all the sequences with keys of a multifastafile



while ($line)
{
	if ($line =~ m/^>(.*)/)
	{
		my $id = $1;
		chomp $id;
	}

	else 
	{
		$sequence{$id} .= $line;
		chomp $sequence{$id};
	}
}

print keys%sequence;

close $in_fh;
