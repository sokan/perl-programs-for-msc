#! /usr/bin/perl
use warnings;

$test = 'tomato hello yellow hallow';

$test =~ /a\b/;

print "Postmatch: $'\n";
print "Prematch: $`\n";
print "Match: $&\n";
