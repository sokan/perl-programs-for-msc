#! /usr/bin/perl
use warnings;

opendir DIR, "." || die "Can't open directory $dir: $!\n";

while(readdir DIR)
{
	print "Found a file: '$_'\n";
}
closedir DIR;
