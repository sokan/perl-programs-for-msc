#! /usr/bin/perl
use warnings;
use strict;

# Hydrophobicity table based on Kyte-Doolittle.
my %hydro = (
	A => 1.800,
	C => 2.500,
	D => -3.500,
	E => -3.500,    
	F => 2.800,     
	G => -0.400,     
	H => -3.200,     
	I => 4.500,    
	K => -3.900,     
	L => 3.800,     
	M => 1.900,     
	N => -3.500,    
	P => -1.600,     
	Q => -3.500,     
	R => -4.500,     
	S => -0.800,    
	T => -0.700,     
	V => 4.200,     
	W => -0.900,     
	Y => -1.300
);	

open my $in_fh, "<", $ARGV[0]
	or die "Could not open '$ARGV[0]' file - $!";

my $seq = '';

while (my $line = <$in_fh>)
{
	if ($line =~ /^>(.*)/)
	{
		my $id = $1;
		chomp $id;
	}
	
	else
	{
		$seq .= $line;
		chomp $seq; #=~ s/\s+//g;;
	}
}

close $in_fh;

my $hydro_sum = 0;

for (my $i = 0; $i <= length($seq) - 5; $i++)
{
	my $average = 0;
	my $window = substr ($seq, $i, 5);
	my @hydro_window = split '', $window;

	for (my $j = 0; $j <= $#hydro_window; $j++)
	{
		$hydro_sum += $hydro{$hydro_window[$j]};    
	}

	$average = $hydro_sum/5;
	$hydro_sum = 0;

}

#print "Hydrophobicity sum = $hydro_sum\n";
#print "Hydrophobicity average = ".($hydro_sum / ($#aas + 1))."\n";
