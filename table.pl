# How many numbers do you want to give? User input
# Save in array and the program will print the average
# of these numbers. Then find the biggest number out
# of the ones given.

#! /usr/bin/perl -w

print "How many numbers do you want to calculate?\n";

$size = <STDIN>;
chomp $size;

print "Enter the numbers you want:\n";
for ($i = 0; $i <= $size - 1; $i++)
{
	chomp ($table[$i] = <STDIN>);
}

print "Your numbers are: @table\n";

$length= @table;
$sum = 0;

for ($i = 0; $i <= $#table; $i++)
{
	$sum = $sum + $table[$i];
}

$average = $sum / $length;

print "The average is $average\n";

for ($i = 0; $i <= $size - 1; $i++)
{
	for ($j = 0; $j <= $size - 1; $j++)
	{
		if ($table[$i] > $table[$i + 1])
		{
			$draft = $table[$i];
			$table[$i] = $table[$i + 1];
			$table[$i + 1] = $draft;
		}
	}
}

print "Sorted table: @table\n";
print "Highest number is: $table[$size]";
