#! /usr/bin/perl
use warnings;

#######################################
# Count the number of lines in a file #
#######################################

$count = 0;

open(INPUT, "<1seq.fasta") || die "Couldn't open file\n";

while (<INPUT>)
{
	$count++;
}

print $count;

close INPUT;
